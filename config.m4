
# vim: tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab

PHP_ARG_ENABLE(autoload, whether to enable autoload support,
[  --enable-autoload     Enable autoload support])

# MAIN -------------------------------------------------------------------------
if test "$PHP_REQUEST" != "no"; then
    PHP_NEW_EXTENSION(autoload, php_autoload.c, $ext_shared, , $PHP_AUTOLOAD_FLAGS)
    PHP_ADD_MAKEFILE_FRAGMENT
fi
