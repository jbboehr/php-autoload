
#ifdef HAVE_CONFIG_H

#include <Zend/zend_compile.h>
#include <Zend/zend.h>
#include "config.h"
#endif

#include "zend_API.h"
#include "zend_interfaces.h"
#include "main/php.h"
#include "main/php_ini.h"
#include "php_main.h"

#include "php_autoload.h"

ZEND_DECLARE_MODULE_GLOBALS(autoload)


struct path_cache_entry {
    zend_string *path;
};


static inline void proxy_call(zend_function * func, zval *return_value, uint32_t param_count, zval *params)
{
    zend_fcall_info fci = {0};
    zend_fcall_info_cache fcic = {0};

    fci.size = sizeof(fci);
    fci.retval = return_value;
    fci.param_count = param_count;
    fci.params = params;
    fci.no_separation = 1;
    ZVAL_UNDEF(&fci.function_name);

    fcic.initialized = 1;
    fcic.function_handler = func;
    fcic.called_scope = zend_get_called_scope(EG(current_execute_data));

    zend_call_function(&fci, &fcic);
}

static inline int my_execute_op_array(zend_op_array *op_array)
{
    zval result = {0};

    if( !op_array ) {
        return FAILURE;
    }

    ZVAL_UNDEF(&result);
    zend_execute(op_array, &result);
    destroy_op_array(op_array);
    efree(op_array);

    if( !EG(exception) ) {
        zval_ptr_dtor(&result);
    }

    return SUCCESS;
}

static inline int my_quick_include_file(zend_string *class_file)
{
    zval filename;
    zend_file_handle file_handle = {0};
    zend_op_array *new_op_array;
    zval result = {0};

    if( zend_hash_exists(&EG(included_files), class_file) ) {
        return FAILURE;
    }

    ZVAL_STR(&filename, class_file);
    new_op_array = compile_filename(ZEND_INCLUDE, &filename);
    return my_execute_op_array(new_op_array);
}

static inline int my_include_file(zend_string *class_file)
{
    int ret;
    zend_file_handle file_handle = {0};
    zend_op_array *new_op_array;
    zend_string *opened_path;
    zval dummy = {0};
    zval result = {0};

    ret = php_stream_open_for_zend_ex(ZSTR_VAL(class_file), &file_handle, USE_PATH | STREAM_OPEN_FOR_INCLUDE);
    if( ret == SUCCESS ) {
        if( !file_handle.opened_path ) {
            file_handle.opened_path = zend_string_dup(class_file, 0);
        }
        opened_path = zend_string_copy(file_handle.opened_path);
        ZVAL_NULL(&dummy);
        if (zend_hash_add(&EG(included_files), opened_path, &dummy)) {
            new_op_array = zend_compile_file(&file_handle, ZEND_REQUIRE);
            zend_destroy_file_handle(&file_handle);
        } else {
            new_op_array = NULL;
            zend_file_handle_dtor(&file_handle);
        }
        zend_string_release(opened_path);
        my_execute_op_array(new_op_array);
    }
    return ret;
}

static inline int my_autoloader(zend_string *class_name, zend_string *lc_class)
{
    zend_string *file_name = zend_string_alloc(ZSTR_LEN(class_name) + 4, 0);
    register char *r = ZSTR_VAL(file_name);
    register char *r_end = r + ZSTR_LEN(file_name);

    memcpy(r, ZSTR_VAL(class_name), ZSTR_LEN(class_name));
    memcpy(r + ZSTR_LEN(class_name), ".php", 4);
    ZSTR_VAL(file_name)[ZSTR_LEN(file_name)] = 0;

    for( ; r <= r_end; r++ ) {
        if( *r == '_' || *r == '\\' ) {
            *r = '/';
        }
    }

    if( my_include_file(file_name) == SUCCESS ) {
        if( zend_hash_exists(EG(class_table), lc_class) ) {
            return SUCCESS;
        }
    }

    return FAILURE;
}

static void path_cache_entry_dtor(zval *entry)
{
    if( Z_TYPE_P(entry) != IS_PTR ) {
        zend_error(E_CORE_WARNING, "autoload path cache entry not a pointer");
        return;
    }

    struct path_cache_entry *pc = Z_PTR_P(entry);
    zend_string_release(pc->path);
}

PHP_FUNCTION(autoload_call)
{
    zend_string *class_name;
    zend_string *lc_class;
    zend_string *filename;
    zval params[1] = {{0}};
    struct path_cache_entry *pce;
    zval *found;
    zend_class_entry *ce;
    struct path_cache_entry tmp_pce = {0};

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_STR(class_name)
    ZEND_PARSE_PARAMETERS_END();

    lc_class = zend_string_tolower(class_name);

    // Lookup in cache
    pce = zend_hash_find_ptr(&AUTOLOAD_G(path_cache), lc_class);
    if( pce ) {
        if( my_quick_include_file(pce->path) == SUCCESS ) {
            return; // done
        } else {
            // if it fails, invalidate the entry
            zend_hash_del(&AUTOLOAD_G(path_cache), lc_class);
        }
    }

    // Use spl autoloader
    ZVAL_STRINGL(&params[0], ZSTR_VAL(class_name), ZSTR_LEN(class_name));
    proxy_call(AUTOLOAD_G(prev_spl_autoload_call), return_value, 1, params);
    zval_dtor(&params[0]);

    // Try our custom autoloader
    if( !zend_hash_exists(EG(class_table), lc_class) ) {
        my_autoloader(class_name, lc_class);
    }

    // Store the class's path in the path cache
    ce = zend_hash_find_ptr(EG(class_table), lc_class);
    if( ce && ce->type == ZEND_USER_CLASS ) {
        filename = ce->info.user.filename;
        if( filename ) {
            tmp_pce.path = zend_string_dup(filename, 1);
            zend_hash_str_add_mem(&AUTOLOAD_G(path_cache), ZSTR_VAL(lc_class), ZSTR_LEN(lc_class), &tmp_pce, sizeof(struct path_cache_entry));
        }
    }
}

PHP_FUNCTION(autoload_register)
{
    zend_function *func = AUTOLOAD_G(prev_spl_autoload_register);
    zval *callable;
    zend_bool a;
    zend_bool b;
    zval params[3];

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "|zbb", &callable, &a, &b) == FAILURE) {
        return;
    }

    if( ZEND_NUM_ARGS() >= 1 ) {
        ZVAL_COPY_VALUE(&params[0], callable);
    }
    if( ZEND_NUM_ARGS() >= 2 ) {
        ZVAL_BOOL(&params[1], a);
    }
    if( ZEND_NUM_ARGS() >= 3 ) {
        ZVAL_BOOL(&params[2], a);
    }

    proxy_call(func, return_value, ZEND_NUM_ARGS(), params);

    EG(autoload_func) = AUTOLOAD_G(my_autoload_call);
}

PHP_FUNCTION(autoload_dump)
{
    HashTable *ht = &AUTOLOAD_G(path_cache);
    HashPosition pos = 0;
    zend_string *key;
    zend_ulong index;
    zval *value;
    struct path_cache_entry *pce;

    array_init(return_value);

    zend_hash_internal_pointer_reset_ex(ht, &pos);

    while( zend_hash_has_more_elements_ex(ht, &pos) == SUCCESS ) {
        zend_hash_get_current_key_ex(ht, &key, &index, &pos);
        value = zend_hash_get_current_data_ex(ht, &pos);

        if( Z_TYPE_P(value) != IS_PTR ) {
            zend_error(E_CORE_WARNING, "autoload path cache entry not a pointer");
        } else if( !key ) {
            zend_error(E_CORE_WARNING, "autoload path cache entry key not a string");
        } else {
            pce = Z_PTR_P(value);
            add_assoc_str_ex(return_value, ZSTR_VAL(key), ZSTR_LEN(key), zend_string_dup(pce->path, 0));
        }

        zend_hash_move_forward_ex(ht, &pos);
    }
}

PHP_FUNCTION(autoload_flush)
{
    zend_hash_destroy(&AUTOLOAD_G(path_cache));
    zend_hash_init(&AUTOLOAD_G(path_cache), 50, NULL, path_cache_entry_dtor, 1);
}

/* {{{ PHP_RINIT_FUNCTION */
static PHP_RINIT_FUNCTION(autoload)
{
    // Set our autoload function
    EG(autoload_func) = AUTOLOAD_G(my_autoload_call);

    // Try to include a composer autoloader
    zend_string * composer_file = zend_string_init(ZEND_STRL("vendor/autoload.php"), 0);
    my_include_file(composer_file);
    zend_string_release(composer_file);

    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINIT_FUNCTION */
static PHP_MINIT_FUNCTION(autoload)
{
    zend_function *tmp;
    size_t tmp_s;

    // Backup spl_autoload_call
    tmp = zend_hash_str_find_ptr(CG(function_table), "spl_autoload_call", sizeof("spl_autoload_call") - 1);
    tmp_s = tmp->type == ZEND_USER_FUNCTION ? sizeof(zend_function) : sizeof(zend_internal_function);
    function_add_ref(tmp);
    AUTOLOAD_G(prev_spl_autoload_call) = pecalloc(1, tmp_s, 1);
    memcpy(AUTOLOAD_G(prev_spl_autoload_call), tmp, tmp_s);

    // Backup spl_autoload_register
    tmp = zend_hash_str_find_ptr(CG(function_table), "spl_autoload_register", sizeof("spl_autoload_register") - 1);
    tmp_s = tmp->type == ZEND_USER_FUNCTION ? sizeof(zend_function) : sizeof(zend_internal_function);
    function_add_ref(tmp);
    AUTOLOAD_G(prev_spl_autoload_register) = pecalloc(1, tmp_s, 1);
    memcpy(AUTOLOAD_G(prev_spl_autoload_register), tmp, tmp_s);

    // Replace spl_autoload_register
    tmp = zend_hash_str_find_ptr(CG(function_table), "autoload_register", sizeof("autoload_register") - 1);
    tmp_s = tmp->type == ZEND_USER_FUNCTION ? sizeof(zend_function) : sizeof(zend_internal_function);
    function_add_ref(tmp);
    zend_hash_str_update_mem(CG(function_table), "spl_autoload_register", sizeof("spl_autoload_register") - 1, tmp, tmp_s);

    // Get a pointer to our call
    tmp = zend_hash_str_find_ptr(CG(function_table), "autoload_call", sizeof("autoload_call") - 1);
    tmp_s = tmp->type == ZEND_USER_FUNCTION ? sizeof(zend_function) : sizeof(zend_internal_function);
    function_add_ref(tmp);
    AUTOLOAD_G(my_autoload_call) = pecalloc(1, tmp_s, 1);
    memcpy(AUTOLOAD_G(my_autoload_call), tmp, tmp_s);

    // Initialize path cache
    zend_hash_init(&AUTOLOAD_G(path_cache), 50, NULL, path_cache_entry_dtor, 1);

    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINIT_FUNCTION */
static PHP_MINFO_FUNCTION(autoload)
{
    char buffer[128];

    php_info_print_table_start();
    php_info_print_table_row(2, "Version", PHP_AUTOLOAD_VERSION);
    php_info_print_table_end();

    php_info_print_table_start();

    snprintf(buffer, sizeof(buffer), "%u", (unsigned) zend_array_count(&AUTOLOAD_G(path_cache)));
    php_info_print_table_row(2, "Current Path Cache Entries", buffer);

    php_info_print_table_end();
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION */
static PHP_MSHUTDOWN_FUNCTION(autoload)
{
    // The strings appear to have already been freed here, so this segfaults
    //zend_hash_destroy(&AUTOLOAD_G(path_cache));
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_GINIT_FUNCTION */
static PHP_GINIT_FUNCTION(autoload)
{
    autoload_globals->prev_autoload_func = NULL;
    autoload_globals->my_autoload_call = NULL;
    autoload_globals->prev_spl_autoload_call = NULL;
    autoload_globals->prev_spl_autoload_register = NULL;
}
/* }}} */

/* {{{ request_deps */
static const zend_module_dep autoload_deps[] = {
    ZEND_MOD_REQUIRED("spl")
    ZEND_MOD_END
};
/* }}} */

/* {{{ spl_functions
 */
const zend_function_entry autoload_functions[] = {
    PHP_FE(autoload_register, NULL)
    PHP_FE(autoload_call, NULL)
    PHP_FE(autoload_dump, NULL)
    PHP_FE(autoload_flush, NULL)
    PHP_FE_END
};
/* }}} */

zend_module_entry autoload_module_entry = {
    STANDARD_MODULE_HEADER_EX, NULL,
    autoload_deps,                      /* Deps */
    PHP_AUTOLOAD_NAME,                  /* Name */
    autoload_functions,                 /* Functions */
    PHP_MINIT(autoload),                /* MINIT */
    PHP_MSHUTDOWN(autoload),            /* MSHUTDOWN */
    PHP_RINIT(autoload),                /* RINIT */
    NULL,                               /* RSHUTDOWN */
    PHP_MINFO(autoload),                /* MINFO */
    PHP_AUTOLOAD_VERSION,               /* Version */
    PHP_MODULE_GLOBALS(autoload),
    PHP_GINIT(autoload),
    NULL,
    NULL,
    STANDARD_MODULE_PROPERTIES_EX
};

#ifdef COMPILE_DL_AUTOLOAD
ZEND_GET_MODULE(autoload)      // Common for all PHP extensions which are build as shared modules
#endif

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: fdm=marker
 * vim: et sw=4 ts=4
 */
