
#ifndef PHP_AUTOLOAD_H
#define PHP_AUTOLOAD_H

#define PHP_AUTOLOAD_NAME "autoload"
#define PHP_AUTOLOAD_VERSION "0.1.3"

extern zend_module_entry autoload_module_entry;
#define phpext_autoload_ptr &autoload_module_entry

ZEND_BEGIN_MODULE_GLOBALS(autoload)
    zend_function *prev_autoload_func;
    zend_function *prev_spl_autoload_call;
    zend_function *prev_spl_autoload_register;
    zend_function *my_autoload_call;
    HashTable path_cache;
ZEND_END_MODULE_GLOBALS(autoload)

ZEND_EXTERN_MODULE_GLOBALS(autoload)
#define AUTOLOAD_G(v) ZEND_MODULE_GLOBALS_ACCESSOR(autoload, v)

#endif /* PHP_AUTOLOAD_H */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: fdm=marker
 * vim: et sw=4 ts=4
 */
