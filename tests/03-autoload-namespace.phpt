--TEST--
autoload with namespace
--SKIPIF--
<?php if( !extension_loaded('autoload') ) die('skip '); ?>
--FILE--
<?php
set_include_path(__DIR__ . '/../fixtures/:' . get_include_path());
spl_autoload_register(function($className) {
    var_dump($className);
});
new TestNs\TestClass2();
autoload_call(TestNs\TestClass2::class); // the function above should get called
autoload_call(TestNs\TestClass2::class); // the function above should get called
var_dump(autoload_dump());
--EXPECTF--
string(17) "TestNs\TestClass2"
string(17) "TestNs\TestClass2"
string(17) "TestNs\TestClass2"
array(%d) {
  ["testns\testclass2"]=>
  string(%d) "%s/fixtures/TestNs/TestClass2.php"
}
