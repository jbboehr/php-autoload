--TEST--
autoload works
--SKIPIF--
<?php if( !extension_loaded('autoload') ) die('skip '); ?>
--FILE--
<?php
set_include_path(__DIR__ . '/../fixtures/:' . get_include_path());
spl_autoload_register(function($className) {
    var_dump($className);
});
new TestClass1();
autoload_call("TestClass1"); // the function above should get called
autoload_call("TestClass1"); // the function above should get called
var_dump(autoload_dump());
--EXPECTF--
string(10) "TestClass1"
string(10) "TestClass1"
string(10) "TestClass1"
array(1) {
  ["testclass1"]=>
  string(%d) "%s/fixtures/TestClass1.php"
}
