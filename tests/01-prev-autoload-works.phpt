--TEST--
prev autoload works
--SKIPIF--
<?php if( !extension_loaded('autoload') ) die('skip '); ?>
--FILE--
<?php
spl_autoload_register(function($className) {
    var_dump($className);
});
try {
    new MissingClass();
    echo 'fail';
} catch( Error $e ) {}
--EXPECT--
string(12) "MissingClass"
