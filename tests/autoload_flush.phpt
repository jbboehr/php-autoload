--TEST--
autoload_flush
--SKIPIF--
<?php if( !extension_loaded('autoload') ) die('skip '); ?>
--FILE--
<?php
set_include_path(__DIR__ . '/../fixtures/:' . get_include_path());
new TestClass1();
autoload_flush();
var_dump(autoload_dump());
--EXPECT--
array(0) {
}
