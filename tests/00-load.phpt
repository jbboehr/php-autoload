--TEST--
load
--SKIPIF--
<?php if( !extension_loaded('autoload') ) die('skip '); ?>
--FILE--
<?php
var_dump(extension_loaded('autoload'));
--EXPECT--
bool(true)
